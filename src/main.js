import Vue from "vue";
import App from "./App.vue";
import { BootstrapVue } from "bootstrap-vue";

Vue.config.productionTip = false;

import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap-vue/dist/bootstrap-vue.min.css";
import "font-awesome/css/font-awesome.min.css";

import store from "./store";
import router from "./router";

Vue.use(BootstrapVue);

Vue.filter("currency", (value) =>
  Intl.NumberFormat("en-US", {
    style: "currency",
    currency: "USD",
  }).format(value)
);

new Vue({
  render: (h) => h(App),
  router,
  store,
}).$mount("#app");
