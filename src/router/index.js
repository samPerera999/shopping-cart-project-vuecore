import Vue from 'vue';
import Router from 'vue-router';

import Pages from "../components/Pages.vue";
import ProductsList from "../components/ProductsList.vue";

Vue.use(Router);

export default new Router({
    mode: 'history',
    routes: [        
        { path: "/:slug?", component: Pages},  
        { path: "/categories/:category?", component: ProductsList},  
        { path: "*", redirect: '/' },      
    ],
  });
