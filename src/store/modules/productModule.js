import Axios from "axios";

const baseUrl = "https://localhost:44365/api";
const pagesUrl = `${baseUrl}/pages`;
const categoriesUrl = `${baseUrl}/categories`;
const productsUrl = `${baseUrl}/products`;
const productImgUrl = "https://localhost:44365/media/products/";

export default {
    namespaced: true,
    strict: true,
    state:{
        pages:[],
        categories:[],
        products:[],
        productImages: productImgUrl,
        currentPage: 1,
        pageCount: 0,
        pageSize: 5,
        currentCategory: "all",        
    },
    mutations:{
        setPages(state, pages){
            state.pages = pages
        },
        setCategories(state, categories){
            state.categories = categories
        },
        setProducts(state, products){
            state.products = products
        },
        setPageCount(state, productCount){
            state.pageCount = Math.ceil(Number(productCount)/state.pageSize);
        },
        setCurrentPage(state, currentPage){
            state.currentPage = currentPage;
        },
        setCurrentCategory(state, category){
            state.currentCategory = category;
        }
    },
    actions:{
        async setPagesActions(context){
            context.commit("setPages",(await Axios.get(pagesUrl)).data)
        },
        async setCategoriesAction(context){
            context.commit("setCategories",(await Axios.get(categoriesUrl)).data)
        },
        async setProductsByCategoryAction(context){
            let url;
            let produtCountUrl; 
            if(this.state.currentCategory != 'all'){
                url = `${productsUrl}/${context.state.currentCategory}?page=${context.state.currentPage}&pageSize=${context.state.pageSize}`;
                produtCountUrl = `${productsUrl}/count/${context.state.currentCategory}`;
            }else{
                url = `${productsUrl}/all?page=${context.state.currentPage}&pageSize=${context.state.pageSize}`;
                produtCountUrl = `${productsUrl}/count/all`;
            }

            let productCount = (await Axios.get(produtCountUrl)).data;

            context.commit("setPageCount",productCount)
            context.commit("setProducts",(await Axios.get(url)).data)
        },
    }
};