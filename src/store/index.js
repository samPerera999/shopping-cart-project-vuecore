import Vue from "vue";
import Vuex from 'vuex';

import productModule from './modules/productModule';
import cartModule from './modules/cartModule';

Vue.use(Vuex);

const store  = new Vuex.Store({
    namespaced: true,
    mutations:{
       
    },
    modules: {
        cartModule : cartModule,
        productModule : productModule,
    }
});

export default store;